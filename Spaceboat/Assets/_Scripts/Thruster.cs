﻿using UnityEngine;

public class Thruster : MonoBehaviour
{
    [SerializeField]
    Rigidbody rBody;
    [SerializeField]
    float thrustForce = 1;
    
    private void Awake()
    {
        if (rBody != null)
            rBody.AddForce(Vector3.zero);

        if (rBody == null)
            rBody = GetComponent<Rigidbody>();
    }
    
    public void ActivateThruster()
    {
        if (rBody == null)
            return;

        //if (turnThruster)
        //    rBody.AddTorque(thrustDirection * thrustForce, ForceMode.Force);
        //else
        //    rBody.AddForce(-transform.forward * thrustForce, ForceMode.Force);

        rBody.AddForceAtPosition(-transform.forward * thrustForce, transform.position, ForceMode.Force);
    }
}
