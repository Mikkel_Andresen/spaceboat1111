﻿using UnityEngine;
using UnityEngine.Events;

public class InputReceiver : MonoBehaviour {

    public enum PressType
    {
        down,
        hold,
        up
    }

    [SerializeField]
    KeyCode key;
    [SerializeField]
    PressType _pressType;

    public UnityEvent[] events;
    public UnityEvent[] downEvents;
    public UnityEvent[] holdEvents;
    public UnityEvent[] upEvents;

    private void Update()
    {
        switch (_pressType)
        {
            case PressType.down:
                if (Input.GetKeyDown(key))
                {
                    RunEvents();
                    RunDownEvents();
                }
                break;
            case PressType.hold:
                if (Input.GetKey(key))
                {
                    RunEvents();
                    RunHoldEvents();
                }
                break;
            case PressType.up:
                if (Input.GetKeyUp(key))
                {
                    RunEvents();
                    RunUpEvents();
                }
                break;
            default:
                break;
        }
    }

    public void RunEvents()
    {
        foreach (UnityEvent ev in events)
        {
            ev.Invoke();
        }
    }
    public void RunDownEvents()
    {
        foreach (UnityEvent ev in downEvents)
        {
            ev.Invoke();
        }
    }
    public void RunHoldEvents()
    {
        foreach (UnityEvent ev in holdEvents)
        {
            ev.Invoke();
        }
    }
    public void RunUpEvents()
    {
        foreach (UnityEvent ev in upEvents)
        {
            ev.Invoke();
        }
    }
}
