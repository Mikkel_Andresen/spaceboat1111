﻿using UnityEngine;

public class GrabTest : MonoBehaviour {
    public Transform objToGrab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space))
        {
            objToGrab.SetParent(transform);
            objToGrab.localPosition = Vector3.zero;
            objToGrab.localRotation = Quaternion.identity;
        }
	}
}
