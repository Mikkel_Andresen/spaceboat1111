﻿using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class ClearPreferences : Editor {
    
    [MenuItem("File/Clear PlayerPrefs")]
    public static void ClearPrefs () {
        PlayerPrefs.DeleteAll();
	}
}
