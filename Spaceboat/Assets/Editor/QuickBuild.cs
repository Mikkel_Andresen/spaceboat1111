﻿using UnityEngine;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;

[ExecuteInEditMode]
public class QuickBuild : EditorWindow
{
    private static string buildName;

    static QuickBuild()
    {
        buildName = "Build " + System.DateTime.Today.Day.ToString("00") + 
            System.DateTime.Today.Month.ToString("00") + 
            System.DateTime.Today.Year.ToString("00");
    }
    

    [MenuItem("File/Save All")]
    public static void QuickSave()
    {
        EditorSceneManager.SaveOpenScenes();
        AssetDatabase.SaveAssets();
        
        //UpVersion();
    }
    

    [MenuItem("File/Save And Exit")]
    public static void SaveExit()
    {
        EditorSceneManager.SaveOpenScenes();
        AssetDatabase.SaveAssets();
        
        //UpVersion();
        
        EditorApplication.Exit(0);
    }


    [MenuItem("File/Quick Build for VR")]
    static void PerformWinBuild_VR()
    {
        // Closes a running build if there is one
        if (System.Diagnostics.Process.GetProcessesByName(buildName) != null)
        {
            foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName(buildName))
            {
                p.Kill();
            }
        }
        if (!EditorApplication.isCompiling)
        {
            Debug.Log("Building for VR");

            // Change player settings for quick testing
            PlayerSettings.displayResolutionDialog = ResolutionDialogSetting.HiddenByDefault;
            PlayerSettings.defaultIsFullScreen = false;
            PlayerSettings.defaultIsNativeResolution = false;
            PlayerSettings.resizableWindow = true;
            PlayerSettings.d3d9FullscreenMode = D3D9FullscreenMode.FullscreenWindow;
            PlayerSettings.d3d11FullscreenMode = D3D11FullscreenMode.FullscreenWindow;
            PlayerSettings.visibleInBackground = true;
            PlayerSettings.defaultScreenWidth = 1024;
            PlayerSettings.defaultScreenHeight = 768;

            // Save scene and project
            EditorSceneManager.SaveOpenScenes();
            AssetDatabase.SaveAssets();

            // Make a build to the build folder in the project
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneWindows64);
            BuildPipeline.BuildPlayer(GetScenePaths(), @"Build/" + buildName + ".exe", BuildTarget.StandaloneWindows64, BuildOptions.AutoRunPlayer);            
        }
        else
        {
            Debug.Log("Compiling... try building again");

        }
    }
    
    
     //[UnityEditor.Callbacks.DidReloadScripts]
    static void UpVersion()
    {
        string[] version = PlayerSettings.bundleVersion.Split('.');
        int v = 0;
        if (version != null && version.Length > 0 && int.TryParse(version[version.Length - 1], out v))
        {
            v++;
        }
        string newVersion = "";
        for (int i = 0; i < version.Length - 1; i++)
        {
            newVersion += version[i] + ".";
        }
        newVersion += v.ToString();
        PlayerSettings.bundleVersion = newVersion;
    }
    
    
    static string GetProjectName()
    {
        string[] s = Application.dataPath.Split('/');
        return s[s.Length - 2];
    }
    

    static string[] GetScenePaths()
    {
        string[] scenes = new string[EditorBuildSettings.scenes.Length];

        for (int i = 0; i < scenes.Length; i++)
        {
            scenes[i] = EditorBuildSettings.scenes[i].path;
        }

        return scenes;
    }
}
